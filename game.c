
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "game.h"
#define ROWS 5
#define COLS 22

cases **initialisation() {
    // Allocation de la mémoire pour les lignes du plateau
    cases **plateau = malloc(ROWS * sizeof(cases*));
    for (int i = 0; i < ROWS; i++) {
        plateau[i] = malloc(COLS * sizeof(cases));
    }

    // Initialisation du plateau avec des zéros
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            plateau[i][j].valeur = 0;
            plateau[i][j].hauteur = 1;
            plateau[i][j].player = -1;
        }
    }

    // Marquage des bords du plateau
    plateau[0][0].valeur=-1;
    plateau[0][1].valeur=-1;
    plateau[0][20].valeur=-1;
    plateau[0][21].valeur=-1;
    //
    plateau[4][0].valeur=-1;
    plateau[4][1].valeur=-1;
    plateau[4][20].valeur=-1;
    plateau[4][21].valeur=-1;
    //
    plateau[1][21].valeur=-1;
    plateau[3][21].valeur=-1;

    for(int i=0;i<ROWS;i++){
        int a;
        if (i%2 == 0){
            a=1;
        }
        else{
            a=0;
        }
        for(int j=a;j<COLS;j+=2){
            plateau[i][j].valeur=-1;
        }
    }

    srand(time(NULL));
    int count = 0;
    int attempts = 0;

    // Placement des pions rouges (3)
    while (count < 3 && attempts < 1000) {
        int i = rand() % ROWS;
        int j = rand() % COLS;
        if (plateau[i][j].valeur == 0) {
            plateau[i][j].valeur = 30; // 30 pour pion rouge
            if (count<2){
                plateau[i][j].player = 0;
            }else{
                plateau[i][j].player = 1;
            }
            ++count;
        }
        attempts++;
    }

    // Placement des pions blancs déplaçables (21) et non déplaçables (20)
    count = 0;
    attempts = 0;
    while (count < 23 && attempts < 1000) {
        int i = rand() % ROWS;
        int j = rand() % COLS;



        if ((((i==0 || i==4) && (j>=2 && j<=19)) || ((i==1 || i==3) && (j==1 || j==19))|| (i==2 && (j==0 || j==21)))&& plateau[i][j].valeur==0) {

            plateau[i][j].valeur = 21;// 21 pour pion blanc déplaçable
            plateau[i][j].player = 0;
             ++count;
        }
        if (plateau[i][j].valeur == 0)
        {
            plateau[i][j].valeur = 20; // 20 pour pion blanc non déplaçable
            plateau[i][j].player = 0;
            ++count;
        }
            attempts++;

    }


    // Placement des pions noirs déplaçables (11) et non déplaçables (10)
    count = 0;
    attempts = 0;
    while (count < 23 && attempts < 1000) {
        int i = rand() % ROWS;
        int j = rand() % COLS;



        if ((((i==0 || i==4) && (j>=2 && j<=19)) || ((i==1 || i==3) && (j==1 || j==19))|| (i==2 && (j==0 || j==21)))&& plateau[i][j].valeur==0) {
            plateau[i][j].valeur = 11;// 21 pour pion blanc déplaçable
            plateau[i][j].player = 1;
        }

        if (plateau[i][j].valeur == 0)
        {
            plateau[i][j].valeur = 10; // 20 pour pion blanc non déplaçable
            plateau[i][j].player = 1;
            ++count;
        }

        attempts++;
    }

    return plateau;
}

void display(cases **m) {
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            if (m[i][j].valeur==-1 && j!=0 && j!=1 && j!=20 && j!=21){

            }else{
                printf("(%d,%d )", m[i][j].valeur,m[i][j].hauteur);

            }
        }
        printf("\n");

    }

    printf("\n");

}

void free_board(cases **board) {
    for (int i = 0; i < ROWS; i++) {
        free(board[i]);
    }
    free(board);
}

int chemin(int i1,int j1,int i2,int j2, int h, cases **plateau)
{
    int r=1;
    if ((j2==j1+h+1) && (i1==i2)) {
        for (int k=j1; k<j2+1; k+=2) {
            if (plateau[i1][k].valeur==0)
                r=0;
        }
    }
    else if (j2==j1-h-1 && i2==i1) {
        for (int k=j2; k<j1+1; k+=2) {
            if (plateau[i1][k].valeur==0)
                r=0;
        }
    }
    else if (j2==j1+h && i2==i1+h) {
        // on parcours la diagonale en bas à droite et on vérifie la présence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i][j].valeur==0) {
                r=0;
                break;
            }
            i++;
            j++;
        }
//        for (int k=j1;  k<j2+1; k++) {
//            for (int i=i1; i<i2+1;++i) {
//                if (plateau[i][k].valeur==0)
//                    r=0;
//            }
//        }
    }
    else if (j2==j1-h && i2==i1-h) {
        // on parcours la diagonale en haut à gauche et on vérifie la présence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i][j].valeur==0) {
                r=0;
                break;
            }
            i--;
            j--;
        }
//        for (int k=j2; k<j1+1; k++) {
//            for (int i=i2; i<i1+1;++i) {
//                if (i>0 && plateau[i][k].valeur==0)
//                    r=0;
//            }
//        }
    }
    else if (j2==j1+h && i2==i1-h) {
        // on parcours la diagonale en haut à droite et on vérifie la présence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i][j].valeur==0) {
                r=0;
                break;
            }
            i--;
            j++;
        }
//        for (int k=j1; k<j2+1; k++) {
//            for (int i=i2; i<i1+1;++i) {
//                if (i>0 && plateau[i][k].valeur==0)
//                    r=0;
//            }
//        }
    }
    else if (j2==j1-h && i2==i1+h) {
        // on parcours la diagonale en bas à gauche et on vérifie la présence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i][j].valeur==0) {
                r=0;
                break;
            }
            i++;
            j--;
        }
//        for (int k=j2; k<j1+1; k++) {
//            for (int i=i1; i<i2+1;++i) {
//                if (plateau[i][k].valeur==0)
//                    r=0;
//            }
//        }
    }
    return r;
}

int **positions(int i,int j, cases **plateau)
{
    // j'ai mis 7 pour mettre la position de base en premier et la garder à porter de main
    int **pos=malloc(7*sizeof(int *));
    for (int i = 0; i < 7; i++) {
        pos[i] = malloc(2 * sizeof(int));
    }
    pos[0][0]=i;
    pos[0][1]=j;
    int h = plateau[i][j].hauteur;
    int a=1;
    if(plateau[i][j].valeur%10==1)
    {

        if(chemin(i,j,i,j+h+1, h, plateau) && j+h+1<COLS && plateau[i][j+h+1].valeur!=-1)  //mouvement droit possible
        {

            if(a<7){
                pos[a][0]=i;
                pos[a][1]=j+h+1;
                ++a;
            }
            else
                printf("erreur\n");

        }
        if(chemin(i,j,i,j-h-1, h, plateau) && j-h-1>0 && plateau[i][j-h-1].valeur!=-1)  //mouvement gauche possible
        {
            if(a<7){
                pos[a][0]=i;
                pos[a][1]=j-h-1;
                ++a;
            }
            else
                printf("erreur\n");
        }
        if(chemin(i,j,i+h,j+h, h, plateau) && j+h<22 && i+h<5 && plateau[i+h][j+h].valeur!=-1)  //mouvement bas droite possible
        {
            if(a<6){
                pos[a][0]=i+h;
                pos[a][1]=j+h;
                ++a;
            }
            else
                printf("erreur\n");
        }
        if(chemin(i,j,i+h,j-h, h, plateau) && i+h<5 && j-h>=0 && i>=0 && plateau[i+h][j-h].valeur!=-1)  //mouvement bas gauche possible
        {
            if(a<7){
                pos[a][0]=i+h;
                pos[a][1]=j-h;
                ++a;
            }
            else
                printf("erreur\n");
        }
        if(chemin(i,j,i-h,j+h, h, plateau) && j+h<22 && i-h>=0 && plateau[i-h][j+h].valeur!=-1)  //mouvement haut droit possible
        {
            if(a<7){
                pos[a][0]=i-h;
                pos[a][1]=j+h;
                ++a;
            }
            else
                printf("erreur\n");
        }

        if(chemin(i,j,i-h,j-h, h, plateau) && j-h>=0 && i-h>=0 && plateau[i-h][j-h].valeur!=-1)  //mouvement haut gauche possible
        {
            if(a<7){
                pos[a][0]=i-h;
                pos[a][1]=j-h;
                ++a;
            }
            else
                printf("erreur\n");
        }
        ++h;
    }
    for (i=a;i<7;i++){
        pos[i][0]=0;
        pos[i][1]=0;
    }

    return pos;
}

void choisis_joue(int**pos,cases** plateau){
    cases play=plateau[pos[0][0]][pos[0][1]];
    int choice=-1;
    for(int i=1;i<7;i++){
        cases before;
        before.valeur=-1;
        before.hauteur=0;
        cases current = plateau[pos[i][0]][pos[i][1]];
        if (current.valeur!=-1){
            if(current.valeur!=play.valeur){
                plateau[pos[i][0]][pos[i][1]].hauteur+=play.hauteur;
                plateau[pos[i][0]][pos[i][1]].valeur=play.valeur;
                choice =-1;
                break;
            } else{
                choice=i;
            }
        }
    }
    if (choice!=-1){
        plateau[pos[choice][0]][pos[choice][1]].hauteur+=play.hauteur;
        plateau[pos[choice][0]][pos[choice][1]].valeur=play.valeur;
    }
    plateau[pos[0][0]][pos[0][1]].valeur=0;
}