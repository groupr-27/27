#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL_ttf.h>
#include "sdl.h"
#define ROWS 5
#define COLS 22
#include <time.h>
#define max 1
#define min 0

#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080
#define CELL_SIZE_l (WINDOW_WIDTH / 21)
#define CELL_SIZE_h (WINDOW_WIDTH / 11)

cases **initialisation() {
    // Allocation de la mémoire pour les lignes du plateau
    cases **plateau = malloc(ROWS * sizeof(cases*));
    for (int i = 0; i < ROWS; i++) {
        plateau[i] = malloc(COLS * sizeof(cases));
    }

    // Initialisation du plateau avec des zéros
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            plateau[i][j].valeur = 0;
            plateau[i][j].hauteur = 1;
        }
    }

    // Marquage des bords du plateau
    plateau[0][0].valeur=-1;
    plateau[0][1].valeur=-1;
    plateau[0][20].valeur=-1;
    plateau[0][21].valeur=-1;
    //
    plateau[4][0].valeur=-1;
    plateau[4][1].valeur=-1;
    plateau[4][20].valeur=-1;
    plateau[4][21].valeur=-1;
    //
    plateau[1][21].valeur=-1;
    plateau[3][21].valeur=-1;

    for(int i=0;i<ROWS;i++){
        int a;
        if (i%2 == 0){
            a=1;
        }
        else{
            a=0;
        }
        for(int j=a;j<COLS;j+=2){
            plateau[i][j].valeur=-1;
        }
    }

    srand(time(NULL));
    int count = 0;
    int attempts = 0;

    // Placement des pions rouges (3)
    while (count < 3 && attempts < 1000) {
        int i = rand() % ROWS;
        int j = rand() % COLS;
        if (plateau[i][j].valeur == 0) {
            plateau[i][j].valeur = 30; // 30 pour pion rouge
            ++count;
        }
        attempts++;
    }

    // Placement des pions blancs déplaçables (21) et non déplaçables (20)
    count = 0;
    attempts = 0;
    while (count < 23 && attempts < 1000) {
        int i = rand() % ROWS;
        int j = rand() % COLS;



        if ((((i==0 || i==4) && (j>=2 && j<=19)) || ((i==1 || i==3) && (j==1 || j==19))|| (i==2 && (j==0 || j==20)))&& plateau[i][j].valeur==0) {

            plateau[i][j].valeur = 21; // 21 pour pion blanc déplaçable
            ++count;
        }
        if (plateau[i][j].valeur == 0)
        {
            plateau[i][j].valeur = 20; // 20 pour pion blanc non déplaçable

            ++count;
        }
        attempts++;

    }


    // Placement des pions noirs déplaçables (11) et non déplaçables (10)
    count = 0;
    attempts = 0;
    while (count < 23 && attempts < 1000) {
        int i = rand() % ROWS;
        int j = rand() % COLS;



        if ((((i==0 || i==4) && (j>=2 && j<=19)) || ((i==1 || i==3) && (j==1 || j==19))|| (i==2 && (j==0 || j==20)))&& plateau[i][j].valeur==0) {
            plateau[i][j].valeur = 11; // 11 pour pion blanc déplaçable
        }

        if (plateau[i][j].valeur == 0)
        {
            plateau[i][j].valeur = 10; // 10 pour pion blanc non déplaçable
            ++count;
        }

        attempts++;
    }



    return plateau;
}

void end_sdl(char ok, const char* msg, SDL_Window* window, SDL_Renderer* renderer) {
    if (!ok) {
        SDL_Log("%s : %s\n", msg, SDL_GetError());
    }

    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
    }
    if (window != NULL) {
        SDL_DestroyWindow(window);
    }

    IMG_Quit();
    SDL_Quit();

    if (!ok) {
        exit(EXIT_FAILURE);
    }
}
int chemin(int i1,int j1,int i2,int j2, int h, cases **plateau)
{
    int r=1;
    if ((j2==j1+h+1) && (i1==i2)) {
        for (int k=j1; k<j2+1; k+=2) {
            if (plateau[i1][k].valeur==0)
                r=0;
        }
    }
    else if (j2==j1-h-1 && i2==i1) {
        for (int k=j2; k<j1+1; k+=2) {
            if (plateau[i1][k].valeur==0)
                r=0;
        }
    }
    else if (j2==j1+h && i2==i1+h) {
        // on parcours la diagonale en bas à droite et on vérifie la présence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i][j].valeur==0) {
                r=0;
                break;
            }
            i++;
            j++;
        }
// for (int k=j1; k<j2+1; k++) {
// for (int i=i1; i<i2+1;++i) {
// if (plateau[i][k].valeur==0)
// r=0;
// }
// }
    }
    else if (j2==j1-h && i2==i1-h) {
        // on parcours la diagonale en haut à gauche et on vérifie la présence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i][j].valeur==0) {
                r=0;
                break;
            }
            i--;
            j--;
        }
// for (int k=j2; k<j1+1; k++) {
// for (int i=i2; i<i1+1;++i) {
// if (i>0 && plateau[i][k].valeur==0)
// r=0;
// }
// }
    }
    else if (j2==j1+h && i2==i1-h) {
        // on parcours la diagonale en haut à droite et on vérifie la présence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i][j].valeur==0) {
                r=0;
                break;
            }
            i--;
            j++;
        }
// for (int k=j1; k<j2+1; k++) {
// for (int i=i2; i<i1+1;++i) {
// if (i>0 && plateau[i][k].valeur==0)
// r=0;
// }
// }
    }
    else if (j2==j1-h && i2==i1+h) {
        // on parcours la diagonale en bas à gauche et on vérifie la présence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i][j].valeur==0) {
                r=0;
                break;
            }
            i++;
            j--;
        }
// for (int k=j2; k<j1+1; k++) {
// for (int i=i1; i<i2+1;++i) {
// if (plateau[i][k].valeur==0)
// r=0;
// }
// }
    }
    return r;
}



void hauteur (char s[50],int x,int y,SDL_Renderer *renderer,SDL_Window *window)
{

    if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);

    TTF_Font* font = NULL; // la variable 'police de caractère'
    font = TTF_OpenFont("Pacifico.ttf", 65); // La police à charger, la taille désirée
    if (font == NULL) end_sdl(0, "Can't load font", window, renderer);

    TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD); // en italique, gras

    SDL_Color color = {255, 0, 0, 255}; // la couleur du texte
    SDL_Surface* text_surface = NULL; // la surface (uniquement transitoire)
    text_surface = TTF_RenderText_Blended(font, s,color); // création du texte dans la surface
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = NULL; // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    SDL_FreeSurface(text_surface); // la texture ne sert plus à rien

    SDL_Rect pos = {x+CELL_SIZE_l /6, y+CELL_SIZE_l, CELL_SIZE_l /4, CELL_SIZE_h/4}; // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h); // récupération de la taille (w, h) du texte
    SDL_RenderCopy(renderer, text_texture, NULL, &pos); // Ecriture du texte dans le renderer
    SDL_DestroyTexture(text_texture); // On n'a plus besoin de la texture avec le texte

    SDL_RenderPresent(renderer); // Affichage


    // TTF_Quit();
}


SDL_Texture* load_texture_from_image(char *file_image_name, SDL_Renderer *renderer) {
    SDL_Surface *my_image = NULL;
    SDL_Texture *my_texture = NULL;

    my_image = IMG_Load(file_image_name);
    if (my_image == NULL) {
        SDL_Log("Unable to load image %s: %s", file_image_name, IMG_GetError());
        return NULL;
    }

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL) {
        SDL_Log("Unable to create texture from %s: %s", file_image_name, SDL_GetError());
        return NULL;
    }

    return my_texture;
}

void render_game(SDL_Renderer *renderer, cases **m) {
    SDL_Texture *jet_noir = load_texture_from_image("n.bmp", renderer);
    if (jet_noir == NULL) {
        end_sdl(0, "Failed to load sprite texture", NULL, renderer);
    }
    SDL_Texture *jet_blanc = load_texture_from_image("b.bmp", renderer);
    if (jet_blanc == NULL) {
        end_sdl(0, "Failed to load sprite texture", NULL, renderer);
    }
    SDL_Texture *jet_rouge = load_texture_from_image("r.bmp", renderer);
    if (jet_rouge == NULL) {
        end_sdl(0, "Failed to load sprite texture", NULL, renderer);
    }

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    // Rendre le plateau
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    for (int i = 0; i <= 20; ++i) {
        SDL_RenderDrawLine(renderer, i * CELL_SIZE_l, 0, i * CELL_SIZE_l, WINDOW_HEIGHT);
        SDL_RenderDrawLine(renderer, 0, i * CELL_SIZE_h, WINDOW_WIDTH, i * CELL_SIZE_h); //////création hexag à faire .....
    }

    // Rendre les pièces
    for (int y = 0; y < 5; y++) {
        for (int x = 0; x < 21; x++) {
            SDL_Rect rect = {x * CELL_SIZE_l, y * CELL_SIZE_h, CELL_SIZE_l, CELL_SIZE_h};
            switch (m[y][x].valeur / 10) {
                case 2:
                    SDL_RenderCopy(renderer, jet_blanc, NULL, &rect);
                    break;
                case 1:
                    SDL_RenderCopy(renderer, jet_noir, NULL, &rect);
                    break;
                case 3:
                    SDL_RenderCopy(renderer, jet_rouge, NULL, &rect);
                    break;
                default:
                    break;
            }
        }
    }

    SDL_RenderPresent(renderer);

    // Clean up textures after rendering
    SDL_DestroyTexture(jet_noir);
    SDL_DestroyTexture(jet_blanc);
    SDL_DestroyTexture(jet_rouge);
}

void init_SDL(SDL_Window **window, SDL_Renderer **renderer) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "SDL ne peut pas être initialisé ! SDL_Error: %s\n", SDL_GetError());
        exit(1);
    }
    *window = SDL_CreateWindow("dvonn", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    if (*window == NULL) {
        fprintf(stderr, "La fenêtre ne peut pas être créée ! SDL_Error: %s\n", SDL_GetError());
        SDL_Quit();
        exit(1);
    }
    *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (*renderer == NULL) {
        end_sdl(0, "Unable to create renderer", *window, NULL);
    }

    // Initialize SDL_image for loading PNGs
    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)) {
        end_sdl(0, "IMG_Init: Failed to initialize PNG support", *window, *renderer);
    }
}

// Variables pour la sélection de pièces


void handle_events1(SDL_Renderer * renderer,SDL_Window * window,cases **m) {
    int selected_x = -1;
    int selected_y = -1;

    SDL_Event event;
    while (SDL_PollEvent(&event) != 0) {
        if (event.type == SDL_QUIT) {
//leanup_SDL();
            exit(0);
        }
// Gérer les autres événements (par exemple, les clics de souris)
        if (event.type == SDL_MOUSEBUTTONDOWN) {
            int x = event.button.x / CELL_SIZE_l;
            int y = event.button.y / CELL_SIZE_h;
            if (selected_x == -1 && selected_y == -1) {
                printf("Clic détecté aux coordonnées : (%d, %d)\n", x, y);
// Premier clic, sélection de la pièce
                if ((m[y][x].valeur !=0 ) && (
                        ((m[y][x].valeur % 10 == 1 ) && m[y][x].player == max) ||
                        ((m[y][x].valeur % 10 == 1 ) && m[y][x].player == min))) {
                    selected_x = x;
                    selected_y = y;
                }
            } /*else {*/
// Deuxième clic, tentative de déplacement
            if (chemin(selected_y, selected_x, y, x,m[selected_y][selected_x].hauteur,m)) {
                selected_y=y;
                selected_x =x;
                SDL_Renderer *ren=SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
                SDL_SetRenderDrawColor(ren,255,255,255,255);
                SDL_Rect rect;
                rect.x=selected_x;
                rect.y=selected_y;
                rect.w=CELL_SIZE_l;
                rect.h=CELL_SIZE_h;
                SDL_RenderFillRect(ren,&rect);
                char s[50];
                sprintf(s, "%d\n", m[y][x].hauteur);
                hauteur(s,x,y,renderer,window);
                SDL_RenderPresent(ren);
            }
            selected_x = -1;
            selected_y = -1; } } }
// Mettre à jour l'état du jeu en fonction des entrées utilisateur
////////////////////////////////////////////////////////////////////////////////////////////////

void handle_events2(cases **m) {
    int selected_x = -1;
    int selected_y = -1;
    SDL_Event event;
    while (SDL_PollEvent(&event) != 0) {
        if (event.type == SDL_QUIT) {
//cleanup_SDL();
            exit(0);
        }



        // Premier clic pour sélectionner la pièce à déplacer
        if (event.type == SDL_MOUSEBUTTONDOWN && selected_x == -1 && selected_y == -1) {
            int x = event.button.x / CELL_SIZE_l;
            int y = event.button.y / CELL_SIZE_h;

            if ((m[y][x].valeur != 0) && (
                    (m[y][x].player == max &&
                     (m[y][x].valeur % 10==1)) ||
                    (m[y][x].player == min && m[y][x].valeur % 10 == 1))) {
                selected_x = x;
                selected_y = y;
                printf("Pièce sélectionnée aux coordonnées : (%d, %d)\n", x, y);
            }
        }

            // Deuxième clic pour choisir la position de destination
        else if (event.type == SDL_MOUSEBUTTONDOWN && selected_x != -1 && selected_y != -1) {
            int x = event.button.x / CELL_SIZE_l;
            int y = event.button.y / CELL_SIZE_h;

            printf("Tentative de déplacement de (%d, %d) à (%d, %d)\n", selected_x, selected_y, x, y);
            if (chemin(selected_y, selected_x, y, x,m[selected_y][selected_x].hauteur,m)) {

                selected_y=y;
                selected_x =x;




                printf("Déplacement effectué de (%d, %d) à (%d, %d)\n", selected_x, selected_y, x, y);

                // Réinitialiser la sélection après le déplacement
                selected_x = -1;
                selected_y = -1;

                /*// Vérifier les conditions de fin de jeu
                int winner = checkWin(state);
                if (winner != EMPTY) {
                if (winner == PLAYER) {
                printf("Les défenseurs ont gagné!\n");
                } else if (winner == OPPONENT) {
                printf("Les attaquants ont gagné!\n");
                }
                cleanup_SDL();
                exit(0);
                } else if (!isMovesLeft(state)) {
                printf("Aucun mouvement restant. Match nul!\n");
                cleanup_SDL();
                exit(0);
                }*/

                // Changer de joueur
                m[y][x].player = (m[y][x].player == min) ? min : max;
            }
            else {
                // Déplacement invalide, réinitialiser la sélection
                printf("Déplacement invalide de (%d, %d) à (%d, %d)\n", selected_x, selected_y, x, y);
                selected_x = -1;
                selected_y = -1;
            }
        }
    }

}



int main() {
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;

    // Initialize SDL and create the window and renderer
    init_SDL(&window, &renderer);

    // Variables pour la sélection de pièces


    // Example piece positions for testing
    /* m[0][0].valeur = 20; // White piece
    m[1][1].valeur = 10; // Black piece
    m[2][2].valeur = 30; // Red piece*/

    cases **m = initialisation();
    // Render the game
    render_game(renderer, m);

    // Wait before exiting
    SDL_Delay(10000);



// let's play
// Gérer les événements SDL (ex : clics de souris, fermeture de fenêtre)



// Nettoyer les ressources SDL avant de quitter le programme
/*void cleanup_SDL() {
SDL_DestroyTexture(jet_noir);
SDL_DestroyTexture(jet_blanc);
SDL_DestroyTexture(jet_rouge);*/



    handle_events1(renderer, window, m);
//handle_events2(m);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    // Clean up
    for (int i = 0; i < 5; ++i) {
        free(m[i]);
    }
    free(m);

    end_sdl(1, "Program finished", window, renderer);

    return 0;
}