#ifndef GAME_H
#define GAME_H

#define ROWS 5
#define COLS 22

typedef struct cases {
    int valeur;
    int hauteur;
    int player;
} cases;

cases **initialisation();
void display(cases **m);
void free_board(cases **board);
int chemin(int i1, int j1, int i2, int j2, int h, cases **plateau);
int **positions(int i, int j, cases **plateau);
void choisis_joue(int **pos, cases **plateau);

#endif



