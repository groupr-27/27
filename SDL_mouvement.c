#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL_ttf.h>
#include "sdl.h"
#include "minmax.h"
#define ROWS 5
#define COLS 22
#include <time.h>
#define max 1
#define min 0

#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080
#define CELL_SIZE_l (WINDOW_WIDTH / 21)
#define CELL_SIZE_h (WINDOW_WIDTH / 11)
const int neighborOffsets2[6][2] = {
        {-1, -1}, {0, -2}, {-1, 1},
        {1, -1}, {0, 2}, {1, 1}
};


void end_sdl(char ok, const char* msg, SDL_Window* window, SDL_Renderer* renderer) {
    if (!ok) {
        SDL_Log("%s : %s\n", msg, SDL_GetError());
    }

    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
    }
    if (window != NULL) {
        SDL_DestroyWindow(window);
    }

    IMG_Quit();
    SDL_Quit();

    if (!ok) {
        exit(EXIT_FAILURE);
    }
}
int chemin_check(int i1,int j1,int i2,int j2, int h, cases **plateau)
{
    int r=1;
    if ((j2==j1+h+1) && (i1==i2)) {
        for (int k=j1; k<j2+1; k+=2) {
            if (plateau[i1][k].valeur==0 || plateau[i1][k].valeur==-1)
                r=0;
        }
    }
    else if (j2==j1-h-1 && i2==i1) {
        for (int k=j2; k<j1+1; k+=2) {
            if (plateau[i1][k].valeur==0 || plateau[i1][k].valeur==-1)
                r=0;
        }
    }
    else if (j2==j1+h && i2==i1+h) {

        // on parcours la diagonale en bas Ã droite et on vÃ©rifie la prÃ©sence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i1][j].valeur==0 || plateau[i1][j].valeur==-1) {
                r=0;
                break;
            }
            i++;
            j++;
        }
    }
    else if (j2==j1-h && i2==i1-h) {
        // on parcours la diagonale en haut Ã gauche et on vÃ©rifie la prÃ©sence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i1][j].valeur==0 || plateau[i1][j].valeur==-1) {
                r=0;
                break;
            }
            i--;
            j--;
        }
    }
    else if (j2==j1+h && i2==i1-h) {
        // on parcours la diagonale en haut Ã droite et on vÃ©rifie la prÃ©sence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i1][j].valeur==0 || plateau[i1][j].valeur==-1) {
                r=0;
                break;
            }
            i--;
            j++;
        }
    }
    else if (j2==j1-h && i2==i1+h) {
        // on parcours la diagonale en bas Ã gauche et on vÃ©rifie la prÃ©sence d'un chemin plein de taille h
        int i = i1;
        int j = j2;
        while(i!=i1 && j!=j2){
            if (plateau[i1][j].valeur==0 || plateau[i1][j].valeur==-1) {
                r=0;
                break;
            }
            i++;
            j--;
        }
    } else{
        r=0;
    }
    return r;
}


void hauteur (char s[50],int x,int y,SDL_Renderer *renderer,SDL_Window *window)
{
    if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);

    TTF_Font* font = NULL; // la variable 'police de caractÃ¨re'
    font = TTF_OpenFont("Pacifico.ttf", 65); // La police Ã charger, la taille dÃ©sirÃ©e
    if (font == NULL) end_sdl(0, "Can't load font", window, renderer);

    TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD); // en italique, gras

    SDL_Color color = {255, 0, 0, 255}; // la couleur du texte
    SDL_Surface* text_surface = NULL; // la surface (uniquement transitoire)
    text_surface = TTF_RenderText_Blended(font, s,color); // crÃ©ation du texte dans la surface
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = NULL; // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface Ã la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);

    SDL_FreeSurface(text_surface); // la texture ne sert plus Ã rien

    SDL_Rect pos = {x, y, 80, 80}; // rectangle oÃ¹ le texte va Ãªtre prositionnÃ©
    if (SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h) != 0) // rÃ©cupÃ©ration de la taille (w, h) du texte
        end_sdl(0, "Can't query texture", window, renderer);
    if (SDL_RenderCopy(renderer, text_texture, NULL, &pos) != 0) // Ecriture du texte dans le renderer
        end_sdl(0, "Can't render texture", window, renderer);

    SDL_DestroyTexture(text_texture); // On n'a plus besoin de la texture avec le texte
    TTF_CloseFont(font); // la police aussi
    TTF_Quit();
}


void affichage2(cases **plateau, SDL_Renderer* renderer, SDL_Texture* blanc, SDL_Texture* noir, SDL_Texture* vide, SDL_Texture* bord, SDL_Texture* pion_rouge, SDL_Texture* pion_blanc, SDL_Texture* pion_noir,SDL_Window* window)
{ SDL_Rect dst = {0};
    SDL_Rect window_dimensions = {0};
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);

    int cell_width = window_dimensions.w / COLS;
    int cell_height = window_dimensions.h / ROWS;

    dst.h = cell_height;
    dst.w = cell_width;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    for (int i = 0; i <= COLS; ++i) {
        SDL_RenderDrawLine(renderer, i * cell_width, 0, i * cell_width, window_dimensions.h);
    }
    for (int i = 0; i <= ROWS; ++i) {
        SDL_RenderDrawLine(renderer, 0, i * cell_height, window_dimensions.w, i * cell_height);
    }

    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            dst.x = j * cell_width;
            dst.y = i * cell_height;

            switch (plateau[i][j].valeur) {
                case -1:
                    SDL_RenderCopy(renderer, bord, NULL, &dst);
                    break;
                case 0:
                    SDL_RenderCopy(renderer, vide, NULL, &dst);
                    break;
                case 10:
                    SDL_RenderCopy(renderer, pion_noir, NULL, &dst);
                    break;
                case 20:
                    SDL_RenderCopy(renderer, pion_blanc, NULL, &dst);
                    break;
                case 11:
                    SDL_RenderCopy(renderer, pion_noir, NULL, &dst);
                    break;
                case 21:
                    SDL_RenderCopy(renderer, pion_blanc, NULL, &dst);
                    break;
                case 30:
                    SDL_RenderCopy(renderer, pion_rouge, NULL, &dst);
                    break;
            }

            if (plateau[i][j].valeur == 10 || plateau[i][j].valeur == 11 || plateau[i][j].valeur == 20 || plateau[i][j].valeur == 21 || plateau[i][j].valeur == 30) {
                char str[50];
                sprintf(str, "%d", plateau[i][j].hauteur);
                hauteur(str, dst.x + dst.w / 2, dst.y, renderer, window);
            }
        }
    }

    SDL_RenderPresent(renderer);
}
int check_move(int i1,int j1,int i2, int j2,cases **plateau,int player){
    int r=1;

    if(plateau[i1][j1].valeur%10!=1 || plateau[i1][j1].player!=player){
        return 0;
    }

    r = chemin_check(i1,j1,i2,j2,plateau[i1][j1].hauteur,plateau);
    return r;
}
int move_piece(cases **plateau, int i1, int j1, int i2, int j2, SDL_Renderer* renderer, SDL_Texture* blanc, SDL_Texture* noir, SDL_Texture* vide, SDL_Texture* bord, SDL_Texture* pion_rouge, SDL_Texture* pion_blanc, SDL_Texture* pion_noir,SDL_Window* window,int player)
{
    // Move the piece
    if (!check_move(i1,j1,i2,j2,plateau,player)){


        return 0;
    }
    plateau[i2][j2].valeur = plateau[i1][j1].valeur;
    plateau[i2][j2].hauteur += plateau[i1][j1].hauteur;
    plateau[i1][j1].valeur = 0; // Clear the original position
    plateau[i2][j2].player = plateau[i1][j1].player;
    for (int i=0;i<6;i++){
        if (i1+neighborOffsets2[i][0]>=0 && j1+neighborOffsets2[i][1]>=0 && i1+neighborOffsets2[i][0]<ROWS && j1+neighborOffsets2[i][1]<COLS &&  plateau[i1+neighborOffsets2[i][0]][j1+neighborOffsets2[i][1]].valeur%10!=1){
            if (plateau[i1+neighborOffsets2[i][0]][j1+neighborOffsets2[i][1]].valeur!=30){
                plateau[i1+neighborOffsets2[i][0]][j1+neighborOffsets2[i][1]].valeur+=1;

            }
        }

    }
    // Re-render the grid
    affichage2(plateau, renderer, blanc, noir, vide, bord, pion_rouge, pion_blanc, pion_noir, window);
    SDL_RenderPresent(renderer);
    return 1;
}

void game_loop(SDL_Window* window, SDL_Renderer* renderer, cases **plateau, SDL_Texture* blanc, SDL_Texture* noir, SDL_Texture* vide, SDL_Texture* bord, SDL_Texture* pion_rouge, SDL_Texture* pion_blanc, SDL_Texture* pion_noir) {
    SDL_Event event;
    int running = 1;
    int click_count = 0;
    int i1, j1, i2, j2;

    while (running) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = 0;
            }
            else if (event.type == SDL_MOUSEBUTTONDOWN) {
                int x = event.button.x;
                int y = event.button.y;
                int i = y / (WINDOW_HEIGHT / ROWS);
                int j = x / (WINDOW_WIDTH / COLS);

                if (click_count == 0) {
                    i1 = i;
                    j1 = j;
                    click_count = 1;
                } else if (click_count == 1) {
                    i2 = i;
                    j2 = j;
                    int joue = move_piece(plateau, i1, j1, i2, j2, renderer, blanc, noir, vide, bord, pion_rouge, pion_blanc, pion_noir, window,0);
                    display(plateau);

                    click_count = 0;
                    if (joue ){
                        cases **bestMove = NULL;
                        choisir_coup(plateau, &bestMove,1);
                        plateau = bestMove;
                        affichage2(plateau, renderer, blanc, noir, vide, bord, pion_rouge, pion_blanc, pion_noir, window);
                    }

                }
            }
        }
    }
}

int main(int argc, char** argv) {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de la SDL : %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_Window* window = SDL_CreateWindow("Test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        fprintf(stderr, "Erreur lors de la creation de la fenetre : %s\n", SDL_GetError());
        SDL_Quit();
        return EXIT_FAILURE;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        fprintf(stderr, "Erreur lors de la creation du renderer : %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return EXIT_FAILURE;
    }

    SDL_Surface* surface_blanc = IMG_Load("bb.bmp");
    SDL_Surface* surface_noir = IMG_Load("bb.bmp");
    SDL_Surface* surface_vide = IMG_Load("bb.bmp");
    SDL_Surface* surface_bord = IMG_Load("bb.bmp");
    SDL_Surface* surface_pion_rouge = IMG_Load("r.bmp");
    SDL_Surface* surface_pion_blanc = IMG_Load("b.bmp");
    SDL_Surface* surface_pion_noir = IMG_Load("n.bmp");

    SDL_Texture* blanc = SDL_CreateTextureFromSurface(renderer, surface_blanc);
    SDL_Texture* noir = SDL_CreateTextureFromSurface(renderer, surface_noir);
    SDL_Texture* vide = SDL_CreateTextureFromSurface(renderer, surface_vide);
    SDL_Texture* bord = SDL_CreateTextureFromSurface(renderer, surface_bord);
    SDL_Texture* pion_rouge = SDL_CreateTextureFromSurface(renderer, surface_pion_rouge);
    SDL_Texture* pion_blanc = SDL_CreateTextureFromSurface(renderer, surface_pion_blanc);
    SDL_Texture* pion_noir = SDL_CreateTextureFromSurface(renderer, surface_pion_noir);

    SDL_FreeSurface(surface_blanc);
    SDL_FreeSurface(surface_noir);
    SDL_FreeSurface(surface_vide);
    SDL_FreeSurface(surface_bord);
    SDL_FreeSurface(surface_pion_rouge);
    SDL_FreeSurface(surface_pion_blanc);
    SDL_FreeSurface(surface_pion_noir);

    cases **plateau = initialisation();

    display(plateau);

    affichage2(plateau, renderer, blanc, noir, vide, bord, pion_rouge, pion_blanc, pion_noir, window);
    SDL_RenderPresent(renderer);

    game_loop(window, renderer, plateau, blanc, noir, vide, bord, pion_rouge, pion_blanc, pion_noir);

    for (int i = 0; i < ROWS; i++) {
        free(plateau[i]);
    }
    free(plateau);

    SDL_DestroyTexture(blanc);
    SDL_DestroyTexture(noir);
    SDL_DestroyTexture(vide);
    SDL_DestroyTexture(bord);
    SDL_DestroyTexture(pion_rouge);
    SDL_DestroyTexture(pion_blanc);
    SDL_DestroyTexture(pion_noir);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}