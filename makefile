# Compiler
CC = gcc

# Executable name
TARGET = interface

# Source files
SRC = SDL_mouvement.c minmax.c game.c

# Object files
OBJ = $(SRC:.c=.o)

# Compiler flags
CFLAGS = -Wall -O2

# Libraries
LIBS = -lm -lSDL2_image -lSDL2_ttf -lSDL2

# Rule to build the executable
$(TARGET): $(OBJ)
	$(CC) -o $@ $^ $(LIBS)

# Rule to build object files
%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

# Clean up build files
.PHONY: clean
clean:
	rm -f $(OBJ) $(TARGET)