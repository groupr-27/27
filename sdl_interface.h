
#ifndef SDL_INTERFACE_H
#define SDL_INTERFACE_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max 1
#define min 0
typedef struct {
    int valeur;
    int hauteur;
    int player;
} cases;


/*typedef struct SDL_Rect
{
 int x, y;
 int w, h;
} SDL_Rect ;*/
int chemin(int i1,int j1,int i2,int j2, int h, cases **plateau);
SDL_Texture* load_texture_from_image(char *file_image_name, SDL_Renderer *renderer);
void render_game(SDL_Renderer *,cases **);
cases **initialisation();
void display(cases **m);
void free_board(cases **board);
int chemin(int i1,int j1,int i2,int j2, int h, cases **plateau);
int **positions(int i,int j, cases **plateau);
void choisis_joue(int**pos,cases** plateau);
void end_sdl(char ok, const char* msg, SDL_Window* window, SDL_Renderer* renderer);
void hauteur (char s[50],int x,int y,SDL_Renderer *renderer,SDL_Window *window);
void handle_events1(SDL_Renderer * renderer,SDL_Window * window,cases **m);
void handle_events2(cases **m);



#endif
