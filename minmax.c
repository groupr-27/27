#include <limits.h>
#include <stdlib.h>
#include "game.h"
#include "minmax.h"

const int neighborOffsets[6][2] = {
        {-1, -1}, {0, -2}, {-1, 1},
        {1, -1}, {0, 2}, {1, 1}
};
int evaluate(cases **board) {
    int score = 0;
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            score += board[i][j].hauteur;
        }
    }
    return score;
}
typedef struct move {
    int xDepart;
    int yDepart;
    int xArrive;
    int yArrive;
    struct move* next;
} move;

void add_move(move **head, int xDepart, int yDepart, int xArrive, int yArrive) {
    move *new_move = (move*)malloc(sizeof(move));
    new_move->xDepart = xDepart;
    new_move->yDepart = yDepart;
    new_move->xArrive = xArrive;
    new_move->yArrive = yArrive;
    new_move->next = *head;
    *head = new_move;
}

void genere_coups(cases **board, move **head,int player) {
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            if (board[i][j].valeur != -1 && board[i][j].valeur != 0 && board[i][j].player==player) {
                int **pos = positions(i, j, board);
                for (int k = 1; k < 7; k++) {
                    if ((pos[k][0] != 0 || pos[k][1] != 0)) {
                        add_move(head, i, j, pos[k][0], pos[k][1]);
                    }
                }
                for (int k = 0; k < 7; k++) {
                    free(pos[k]);
                }
                free(pos);
            }
        }
    }
}

cases **copy_board(cases **original) {
    cases **copy = malloc(ROWS * sizeof(cases*));
    for (int i = 0; i < ROWS; i++) {
        copy[i] = malloc(COLS * sizeof(cases));
        for (int j = 0; j < COLS; j++) {
            copy[i][j] = original[i][j];
        }
    }
    return copy;
}

void free_moves(move *head) {
    move *temp;
    while (head != NULL) {
        temp = head;
        head = head->next;
        free(temp);
    }
}

int minmax(cases **board, int depth, int is_maximizing,int player) {
    if (depth == 0) {
        return evaluate(board);
    }

    move *moves = NULL;
    genere_coups(board, &moves, player);

    int best_value = is_maximizing ? INT_MIN : INT_MAX;
    move *current = moves;

    while (current != NULL) {
        cases **new_board = copy_board(board);
        new_board[current->xArrive][current->yArrive] = new_board[current->xDepart][current->yDepart];
        new_board[current->xArrive][current->yArrive].hauteur+=new_board[current->xDepart][current->yDepart].hauteur;
        new_board[current->xArrive][current->yArrive].player =  new_board[current->xDepart][current->yDepart].player;
        new_board[current->xDepart][current->yDepart].valeur = 0;
        new_board[current->xDepart][current->yDepart].hauteur = 1;

        for (int i=0;i<6;i++){
            if (current->xDepart+neighborOffsets[i][0]>=0 && current->yDepart+neighborOffsets[i][1]>=0 && current->xDepart+neighborOffsets[i][0]<ROWS && current->yDepart+neighborOffsets[i][1]<COLS &&  new_board[current->xDepart+neighborOffsets[i][0]][current->yDepart+neighborOffsets[i][1]].valeur%10!=1){
                if (new_board[current->xDepart+neighborOffsets[i][0]][current->yDepart+neighborOffsets[i][1]].valeur!=30){
                    new_board[current->xDepart+neighborOffsets[i][0]][current->yDepart+neighborOffsets[i][1]].valeur+=1;

                }
            }

        }

        int value = minmax(new_board, depth - 1, !is_maximizing,!player);
        if (is_maximizing) {
            best_value = value > best_value ? value : best_value;
        } else {
            best_value = value < best_value ? value : best_value;
        }

        free_board(new_board);
        current = current->next;
    }

    free_moves(moves);
    return best_value;
}

void choisir_coup(cases **board, cases ***bestMove,int player) {
    int best_value = INT_MIN;
    move *moves = NULL;
    genere_coups(board, &moves,player);

    move *current = moves;

    while (current != NULL) {
        cases **new_board = copy_board(board);
        new_board[current->xArrive][current->yArrive] = new_board[current->xDepart][current->yDepart];
        new_board[current->xArrive][current->yArrive].hauteur+=new_board[current->xDepart][current->yDepart].hauteur;
        new_board[current->xArrive][current->yArrive].player =  new_board[current->xDepart][current->yDepart].player;
        new_board[current->xDepart][current->yDepart].valeur = 0;
        new_board[current->xDepart][current->yDepart].hauteur = 1;

        int value = minmax(new_board, 3, 0,!player);
        if (value > best_value) {
            best_value = value;
            if (*bestMove != NULL) {
                free_board(*bestMove);
            }
            *bestMove = new_board;
        } else {
            free_board(new_board);
        }

        current = current->next;
    }

    free_moves(moves);
}