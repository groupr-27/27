#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL_ttf.h>  
#include "sdl.h"
#define ROWS 5
#define COLS 22
#include <time.h>
#define max 1
#define min 0

#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080
#define CELL_SIZE_l (WINDOW_WIDTH / 21)
#define CELL_SIZE_h (WINDOW_WIDTH / 11)



cases **initialisation() {
    // Allocation de la mémoire pour les lignes du plateau
    cases **plateau = malloc(ROWS * sizeof(cases*));
    for (int i = 0; i < ROWS; i++) {
        plateau[i] = malloc(COLS * sizeof(cases));
    }

    // Initialisation du plateau avec des zéros
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            plateau[i][j].valeur = 0;
            plateau[i][j].hauteur = 1;
            plateau[i][j].player = -1; // No player initially
        }
    }

    // Marquage des bords du plateau
    plateau[0][0].valeur = -1;
    plateau[0][1].valeur = -1;
    plateau[0][20].valeur = -1;
    plateau[0][21].valeur = -1;
    plateau[4][0].valeur = -1;
    plateau[4][1].valeur = -1;
    plateau[4][20].valeur = -1;
    plateau[4][21].valeur = -1;
    plateau[1][21].valeur = -1;
    plateau[3][21].valeur = -1;

    for (int i = 0; i < ROWS; i++) {
        int a = (i % 2 == 0) ? 1 : 0;
        for (int j = a; j < COLS; j += 2) {
            plateau[i][j].valeur = -1;
        }
    }

    srand(time(NULL));
    int count = 0;
    int attempts = 0;

    // Placement des pions rouges (3)
    while (count < 3 && attempts < 1000) {
        int i = rand() % ROWS;
        int j = rand() % COLS;
        if (plateau[i][j].valeur == 0) {
            plateau[i][j].valeur = 30; // 30 pour pion rouge
            plateau[i][j].player = max;
            ++count;
        }
        attempts++;
    }

    // Placement des pions blancs déplaçables (21) et non déplaçables (20)
    count = 0;
    attempts = 0;
    while (count < 23 && attempts < 1000) {
        int i = rand() % ROWS;
        int j = rand() % COLS;

        if ((((i == 0 || i == 4) && (j >= 2 && j <= 19)) || ((i == 1 || i == 3) && (j == 1 || j == 19)) || (i == 2 && (j == 0 || j == 20))) && plateau[i][j].valeur == 0) {
            plateau[i][j].valeur = 21; // 21 pour pion blanc déplaçable
            plateau[i][j].player = max;
            ++count;
        }
        if (plateau[i][j].valeur == 0) {
            plateau[i][j].valeur = 20; // 20 pour pion blanc non déplaçable
            plateau[i][j].player = min;
            ++count;
        }
        attempts++;
    }

    // Placement des pions noirs déplaçables (11) et non déplaçables (10)
    count = 0;
    attempts = 0;
    while (count < 23 && attempts < 1000) {
        int i = rand() % ROWS;
        int j = rand() % COLS;

        if ((((i == 0 || i == 4) && (j >= 2 && j <= 19)) || ((i == 1 || i == 3) && (j == 1 || j == 19)) || (i == 2 && (j == 0 || j == 20))) && plateau[i][j].valeur == 0) {
            plateau[i][j].valeur = 11; // 11 pour pion blanc déplaçable
            plateau[i][j].player = max;
            ++count;
        }
        if (plateau[i][j].valeur == 0) {
            plateau[i][j].valeur = 10; // 10 pour pion blanc non déplaçable
            plateau[i][j].player = min;
            ++count;
        }
        attempts++;
    }

    return plateau;
}

void end_sdl(char ok, const char* msg, SDL_Window* window, SDL_Renderer* renderer) {
    if (!ok) {
        SDL_Log("%s : %s\n", msg, SDL_GetError());
    }

    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
    }
    if (window != NULL) {
        SDL_DestroyWindow(window);
    }

    IMG_Quit();
    SDL_Quit();

    if (!ok) {
        exit(EXIT_FAILURE);
    }
}

int chemin(int i1, int j1, int i2, int j2, int h, cases **plateau) {
    int r = 1;
    if ((j2 == j1 + h + 1) && (i1 == i2)) {
        for (int k = j1; k < j2 + 1; k += 2) {
            if (plateau[i1][k].valeur == 0)
                r = 0;
        }
    } else if (j2 == j1 - h - 1 && i2 == i1) {
        for (int k = j2; k < j1 + 1; k += 2) {
            if (plateau[i1][k].valeur == 0)
                r = 0;
        }
    } else if (j2 == j1 + h && i2 == i1 + h) {
        int i = i1;
        int j = j2;
        while (i != i1 && j != j2) {
            if (plateau[i][j].valeur == 0) {
                r = 0;
                break;
            }
            i++;
            j++;
        }
    } else if (j2 == j1 - h && i2 == i1 - h) {
        int i = i1;
        int j = j2;
        while (i != i1 && j != j2) {
            if (plateau[i][j].valeur == 0) {
                r = 0;
                break;
            }
            i--;
            j--;
        }
    } else if (j2 == j1 + h && i2 == i1 - h) {
        int i = i1;
        int j = j2;
        while (i != i1 && j != j2) {
            if (plateau[i][j].valeur == 0) {
                r = 0;
                break;
            }
            i--;
            j++;
        }
    } else if (j2 == j1 - h && i2 == i1 + h) {
        int i = i1;
        int j = j2;
        while (i != i1 && j != j2) {
            if (plateau[i][j].valeur == 0) {
                r = 0;
                break;
            }
            i++;
            j--;
        }
    }
    return r;
}

void hauteur(char s[50], int x, int y, SDL_Renderer *renderer, SDL_Window *window) {
    if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);

    TTF_Font* font = TTF_OpenFont("Pacifico.ttf", 65);
    if (font == NULL) end_sdl(0, "Can't load font", window, renderer);

    TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);

    SDL_Color color = {255, 0, 0, 255};
    SDL_Surface* text_surface = TTF_RenderText_Blended(font, s, color);
    if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    SDL_FreeSurface(text_surface);

    SDL_Rect pos = {x + CELL_SIZE_l / 6, y + CELL_SIZE_l, CELL_SIZE_l / 4, CELL_SIZE_l / 2};
    if (SDL_RenderCopy(renderer, text_texture, NULL, &pos) != 0) end_sdl(0, "Can't display texture", window, renderer);

    SDL_DestroyTexture(text_texture);
    TTF_CloseFont(font);
    TTF_Quit();
}

void render_game(SDL_Renderer *renderer, cases **m) {
    // Clear the renderer
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // Black background
    SDL_RenderClear(renderer);

    // Render each cell
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            SDL_Rect cell;
            cell.x = j * CELL_SIZE_l;
            cell.y = i * CELL_SIZE_h;
            cell.w = CELL_SIZE_l;
            cell.h = CELL_SIZE_h;

            if (m[i][j].valeur == -1) {
                // Cell is a border
                SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // White
                SDL_RenderFillRect(renderer, &cell);
            } else if (m[i][j].valeur == 30) {
                // Red piece
                SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Red
                SDL_RenderFillRect(renderer, &cell);
            } else if (m[i][j].valeur == 21) {
                // White movable piece
                SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // White
                SDL_RenderFillRect(renderer, &cell);
            } else if (m[i][j].valeur == 20) {
                // White non-movable piece
                SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255); // Light Gray
                SDL_RenderFillRect(renderer, &cell);
            } else if (m[i][j].valeur == 11) {
                // Black movable piece
                SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255); // Dark Gray
                SDL_RenderFillRect(renderer, &cell);
            } else if (m[i][j].valeur == 10) {
                // Black non-movable piece
                SDL_SetRenderDrawColor(renderer, 50, 50, 50, 255); // Very Dark Gray
                SDL_RenderFillRect(renderer, &cell);
            }

            // Draw cell border
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // Black
            SDL_RenderDrawRect(renderer, &cell);
        }
    }

    // Present the renderer
    SDL_RenderPresent(renderer);
}

void handle_events1(SDL_Renderer * renderer, SDL_Window * window, cases **m) {
    int selected_x = -1;
    int selected_y = -1;

    SDL_Event event;
    while (SDL_PollEvent(&event) != 0) {
        if (event.type == SDL_QUIT) {
            exit(0);
        }

        if (event.type == SDL_MOUSEBUTTONDOWN) {
            int x = event.button.x / CELL_SIZE_l;
            int y = event.button.y / CELL_SIZE_h;
            if (selected_x == -1 && selected_y == -1) {
                printf("Clic détecté aux coordonnées : (%d, %d)\n", x, y);
                // Premier clic, sélection de la pièce
                if ((m[y][x].valeur != 0) && (
                    ((m[y][x].valeur % 10 == 1) && m[y][x].player == max) ||
                    ((m[y][x].valeur % 10 == 1) && m[y][x].player == min))) {
                    selected_x = x;
                    selected_y = y;
                }
            } else {
                // Deuxième clic, tentative de déplacement
                if (chemin(selected_y, selected_x, y, x, m[selected_y][selected_x].hauteur, m)) {
                    // Incrémenter la hauteur de la pièce
                    m[y][x].hauteur += m[selected_y][selected_x].hauteur;
                    m[y][x].valeur = m[selected_y][selected_x].valeur;
                    m[y][x].player = m[selected_y][selected_x].player;

                    // Marquer la position initiale comme vide
                    m[selected_y][selected_x].valeur = 0;
                    m[selected_y][selected_x].hauteur = 1;
                    m[selected_y][selected_x].player = -1; // ou un autre indicateur pour "aucun joueur"

                    // Afficher la nouvelle hauteur
                    char s[50];
                    sprintf(s, "%d", m[y][x].hauteur);
                    hauteur(s, x * CELL_SIZE_l, y * CELL_SIZE_h, renderer, window);

                    // Rafraîchir l'affichage du jeu
                    render_game(renderer, m);

                    selected_x = -1;
                    selected_y = -1;
                } else {
                    printf("Déplacement invalide de (%d, %d) à (%d, %d)\n", selected_x, selected_y, x, y);
                    selected_x = -1;
                    selected_y = -1;
                }
            }
        }
    }
}

int main() {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return 1;
    }

    SDL_Window *window = SDL_CreateWindow(
        "Jeu de pions",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        WINDOW_WIDTH, WINDOW_HEIGHT,
        SDL_WINDOW_SHOWN
    );

    if (!window) {
        SDL_Log("Unable to create window: %s", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        SDL_Log("Unable to create renderer: %s", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return 1;
    }

    cases **plateau = initialisation();
    render_game(renderer, plateau);

    while (1) {
        handle_events1(renderer, window, plateau);
    }

    for (int i = 0; i < ROWS; i++) {
        free(plateau[i]);
    }
    free(plateau);

    end_sdl(1, "Normal ending", window, renderer);
    return 0;
}
